FROM ubuntu:18.04 as osxcross

RUN apt-get update \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y --no-install-recommends --assume-yes \
    time \
    git \
    ca-certificates \
    curl \
    clang \
    make \
    cmake \
    patch \
    libxml2-dev \
    libssl-dev;

RUN time git clone https://github.com/tpoechtrager/osxcross \
    && cd "/osxcross" \
    && mkdir -p tarballs \
    && time curl -L https://github.com/phracker/MacOSX-SDKs/releases/download/10.15/MacOSX10.13.sdk.tar.xz > tarballs/MacOSX10.13.sdk.tar.xz \
    && sed -i 's/apple-libtapi.git 1100.0.11/apple-libtapi.git 3cb307764cc5f1856c8a23bbdf3eb49dfc6bea48/g' build.sh \
    && UNATTENDED=yes OSX_VERSION_MIN=10.7 time ./build.sh;

# Only keep target folder
FROM scratch
# NOTE: linking fails if this is copied to a different path than what it was built at
COPY --from=osxcross /osxcross/target/ /osxcross/target/

