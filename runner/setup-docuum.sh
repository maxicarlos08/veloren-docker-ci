#!/bin/bash

# Note: this script can be re-ran to update docuum or to change the cleanup threshold

echo "Docuum will automatically clean up old docker images when the runner machine is low on space. We currently use a 50 GB threshold."
echo "See: https://github.com/stepchowfun/docuum"
read -p "Use docuum (recommended)? This will help keep your runner disk from filling up with old images as we switch to new ones but it will also clean up old unrelated docker images that you create locally. (y/n)?" usechoice

case "$usechoice" in
  y|Y ) echo " >>> starting docuum setup";;
  n|N ) echo " >>> skipping docuum setup" && exit 0;;
  * ) echo "invalid" && exit 1;;
esac

container_name="veloren-docuum"

threshold="50"

read -p "Enter the threshold in GBs for the space docker images are allowed to use before cleanup is triggered or press enter to use the default($threshold):" user_threshold

case "$user_threshold" in
  "" ) echo ">>> using the default";;
  *  ) threshold=$user_threshold;;
esac

read -p "Do you have images that docuum should exclude from clean up. (y/n)?" keepchoice

case "$keepchoice" in
  y|Y ) read -p "Enter the regular express for which to match against repository:tag and prevent deletion of images:" user_regex
        keep_regex="--keep ${user_regex}";;
  n|N ) echo " >>> skipping exclusion setup";;
  * ) echo "invalid" && exit 1;;
esac

echo $keep_regex

echo " >>> stopping container with the name ${container_name} if it already exists"
docker container stop $container_name
docker container rm $container_name
echo " >>> starting new container with the name ${container_name}"
# https://github.com/stepchowfun/docuum#running-docuum-in-a-docker-container
docker run \
  --detach \
  --init \
  --name veloren-docuum \
  --restart always \
  --volume /var/run/docker.sock:/var/run/docker.sock \
  --volume docuum:/root \
  stephanmisc/docuum --threshold "${threshold} GB" $keep_regex &&
echo " >>> docuum setup complete"
